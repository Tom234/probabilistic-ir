from __future__ import print_function
from time import time

from sklearn.feature_extraction.text import CountVectorizer
from sklearn.datasets import fetch_20newsgroups
import numpy as np
from sklearn.cluster import KMeans
from sklearn.mixture import GaussianMixture

np.set_printoptions(threshold=np.nan)


n_samples = 2000
n_features = 1000

# Load the 20 newsgroups dataset and vectorize it. We use a few heuristics
# to filter out useless terms early on: the posts are stripped of headers,
# footers and quoted replies, and common English words, words occurring in
# only one document or in at least 95% of the documents are removed.

print("Loading dataset...")
dataset = fetch_20newsgroups(shuffle=True, random_state=1,
                             remove=('headers', 'footers', 'quotes'))
data_samples = dataset.data[:n_samples]

# Use tf (raw term count) features for LDA.
print("Extracting tf features for LDA...")
tf_vectorizer = CountVectorizer(max_df=0.95, min_df=2,
                                max_features=n_features,
                                stop_words='english')
tf = tf_vectorizer.fit_transform(data_samples)
datas = tf.toarray()

# Guassian Mixture Model:
gmm = GaussianMixture(n_components=15).fit(datas)
# compute soft-clustering assignment:
labels = gmm.predict(datas)


lmda = 40
minimum = 10e10
best_k = 0

for k in range(15, 26):

    # compute k means:
    kmeans = KMeans(n_clusters=k)
    kmeans.fit(datas)

    y_kmeans = kmeans.predict(datas)
    centers = kmeans.cluster_centers_

    # compute RSS
    rss = 0
    for i, vec in enumerate(datas):  # goes through all 2k docs
        rss += ((vec - centers[y_kmeans[i]]) ** 2).sum()

    aic = rss + lmda * k
    if aic < minimum:
        minimum = aic
        best_k = k
    print(aic)
    # rss = ((datas - centers[y_kmeans]) ** 2).sum()

print("the best k is:", best_k)
