from gensim.models import Word2Vec
from sklearn.metrics.pairwise import cosine_similarity
from queue import PriorityQueue


tagdic = {}  # tagid to tagname mapping
with open('dataset/tags.dat') as f:
    lines = f.readlines()

for line in lines[1:]:
    try:
        key, value = line.split()
        tagdic[key] = value
    except ValueError:
        line = line.split()
        if len(line) < 2:
            # missing value
            continue
        if len(line) > 2:
            # tagname consists of more than 1 word
            key = line[0]
            value = ''.join(line[1:])
        tagdic[key] = value


bookdic = {}  # bookmarkid->bookmark mapping
with open('dataset/bookmarks.dat', errors='ignore') as f:
    lines = f.readlines()

for line in lines[1:]:
    line = line.split()
    index = line[0]
    url = line[-3]
    bookdic[index] = url


bookmarktagdic = {}  # bookmarkid to list of tags mapping
with open('dataset/bookmark_tags.dat') as f:
    lines = f.readlines()

for line in lines[1:]:
    bID, tagID, _, = line.split()
    if bID in bookmarktagdic:
        bookmarktagdic[bID].append(tagdic[tagID])
    else:
        bookmarktagdic[bID] = [tagdic[tagID]]


sentences = bookmarktagdic.values()
model = Word2Vec(sentences, size=100, window=5, workers=4, min_count=1)
vectors = model.wv

query = 'firefox addons extensions'
queryvec = 0

for k, word in enumerate(query.split()):
    queryvec += vectors[word]

queryvec /= (k + 1)

queue = PriorityQueue()

for bID, sentence in bookmarktagdic.items():
    sentencevec = 0
    for word in sentence:
        sentencevec += vectors[word]
    sentencevec /= len(sentence)

    sim = cosine_similarity([queryvec], [sentencevec])
    queue.put((-sim[0][0], bID))


print("The top-10 documents for the query 'firefox addons extensions' are:")
for i in range(10):
    sim, bID = queue.get()
    print(bookdic[bID], "\t with cosine similarity of:", -sim)
